import random
import os

def generate_string(chars, len):
    chararr = list(chars)
    secure_random = random.SystemRandom()

    final_str = ""
    for x in range(len+1):
        final_str += secure_random.choice(chararr)

    return final_str

def get_unique_name(extension, config, datadir):
  fn = generate_string(config['generation-characters'], config['filename-length'])
  if os.path.exists(os.path.join(datadir, fn+extension)):
    return get_unique_name(extension, config, datadir)
  else:
    return fn