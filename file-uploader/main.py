from flask import Flask, request, jsonify, send_file
import yaml
import mimetypes
import os
import sys

sys.path.insert(0, os.path.dirname(__file__))
import util
import database

config = {}
datadir = os.path.join(os.path.dirname(__file__), '../www/')

database.initialize_database()

try:
    open(os.path.join(os.path.dirname(__file__), 'config/config.yml'), 'r')
except FileNotFoundError as exc:
    print(exc)
    sys.exit(1)

with open(os.path.join(os.path.dirname(__file__), 'config/config.yml'), 'r') as stream:
    try:
        config = yaml.load(stream)
    except yaml.YAMLError as exc:
        print(exc)
        sys.exit(2)

app = Flask(__name__)


@app.route('/')
def index():
    return 'This is a private file uploading service'


@app.route('/<path:filename>')
def show_image(filename):
    filepath = os.path.join(datadir, filename)
    if os.path.exists(filepath):
      return send_file(filepath), 200
    else:
      return 'REEEEEEsource not found.', 404


@app.route('/api/upload', methods=['POST', 'GET'])
def upload_file():
    if request.method == 'GET':
      return 'Invalid Method.', 405

    auth_header = request.headers['Auth']
    if auth_header == config['auth-secret']:
        file = request.files['image']
        delete = util.generate_string(config['generation-characters'], config['deletion-secret-length'])
        ext = mimetypes.guess_extension(file.mimetype, False)
        fn = util.get_unique_name(ext, config, datadir)
        file.save(os.path.join(datadir, fn + ext), config['file-max-size'] * 1024 * 1024)
        database.insert_image(fn+ext, delete)

        return jsonify({'filename': fn + ext, 'deletionSecret': delete}), 200
    else:
        return 'Unauthorized.', 401


@app.route('/api/delete/<path:filename>')
def delete_file(filename):
  if(not os.path.exists(os.path.join(datadir, filename))):
    return 'File not found.', 404

  if request.args['secret'] == database.get_secret(filename):
    database.remove_image(filename)
    os.remove(os.path.join(datadir,filename))
    return '', 204
  else:
    return 'Unauthorized.', 401
