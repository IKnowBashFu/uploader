import sqlite3

def initialize_database():
  c = sqlite3.connect('database.sqlite')
  cu = c.cursor()
  cu.execute('''CREATE TABLE IF NOT EXISTS images
                  (filename TEXT, deletion_secret TEXT)''')
  c.commit()
  
def insert_image(filename, deletion_secret):
  c = sqlite3.connect('database.sqlite')
  cu = c.cursor()
  cu.execute('INSERT INTO images VALUES (?, ?)', (filename, deletion_secret))
  c.commit()

def remove_image(filename):
  c = sqlite3.connect('database.sqlite')
  cu = c.cursor()
  cu.execute('DELETE FROM images WHERE filename = ?', (filename,))
  c.commit()

def get_secret(filename):
  c = sqlite3.connect('database.sqlite')
  cu = c.cursor()
  cu.execute('SELECT deletion_secret FROM images WHERE filename = ?', (filename,))
  data = cu.fetchone()[0]
  if data is None:
    return False
  else:
    return data