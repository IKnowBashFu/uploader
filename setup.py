from distutils.core import setup

setup(
    name='FileUploader',
    version='0.1.0',
    packages=['file-uploader',],
    license='The Unlicense',
    long_description=open('README.md').read(),
    install_requires=[
        'flask',
        'pyyaml'
    ]
)
