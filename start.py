import argparse
import subprocess
import os

parser = argparse.ArgumentParser(description='Start the development test server')
parser.add_argument('-P', '--production', action='store_true')
parser.add_argument('-p', '--port', action='store', type=int, default=5000)
args = parser.parse_args()

env = dict(os.environ)

env["FLASK_APP"] = "file-uploader/main.py"

if not args.production:
  env["FLASK_ENV"] = "development"

subprocess.Popen(['flask', 'run', '-p'+str(args.port)], env=env).wait()
